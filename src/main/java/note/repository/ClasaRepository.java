package note.repository;

import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.utils.ClasaException;

import java.util.HashMap;
import java.util.List;

public interface ClasaRepository {

    void creazaClasa(List<Elev> elevi, List<Nota> note);

    HashMap<Elev, HashMap<String, List<Double>>> getClasa();

    List<Medie> calculeazaMedii() throws ClasaException;

    void afiseazaClasa();

    List<Corigent> getCorigenti();
}
